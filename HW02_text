package ru.javawebinar.topjava.repository.mock;

import ru.javawebinar.topjava.model.Meal;
import ru.javawebinar.topjava.repository.MealRepository;
import ru.javawebinar.topjava.util.MealsUtil;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryMealRepositoryImpl implements MealRepository {
    private Map<Integer, Meal> repository = new ConcurrentHashMap<>();
    private AtomicInteger counter = new AtomicInteger(0);

    {
        MealsUtil.MEALS.forEach(this::save);
    }

    @Override
    public Meal save(Meal meal) {
        if (meal.isNew()) {
            meal.setId(counter.incrementAndGet());
            repository.put(meal.getId(), meal);
            return meal;
        }
        // treat case: update, but absent in storage
        return repository.computeIfPresent(meal.getId(), (id, oldMeal) -> meal);
    }

    @Override
    public void delete(int id) {
        repository.remove(id);
    }

    @Override
    public Meal get(int id) {
        return repository.get(id);
    }

    @Override
    public Collection<Meal> getAll() {
        return repository.values();
    }
}







Домашнее задание HW02

    1: переименовать MockUserRepositoryImpl в InMemoryUserRepositoryImpl и имплементировать по аналогии с InMemoryMealRepositoryImpl (список пользователей возвращать отсортированным по имени)

    2: сделать Meal extends AbstractBaseEntity, MealWithExceed перенести в пакет ru.javawebinar.topjava.to (transfer objects)
    3: Изменить MealRepository и InMemoryMealRepositoryImpl таким образом, чтобы вся еда всех пользователей находилась в одном общем хранилище, но при этом каждый конкретный авторизованный пользователь мог видеть и редактировать только свою еду.
        3.1: реализовать хранение еды для каждого пользователя можно с добавлением поля userId в Meal ИЛИ без него (как нравится). Напомню, что репозиторий один и приложение может работать одновременно с многими пользователями.
        3.2: если по запрошенному id еда отсутствует или чужая, возвращать null/false (см. комментарии в UserRepository)
        3.3: список еды возвращать отсортированный в обратном порядке по датам
        3.4: атомарность операций не требуется (коллизии при одновременном изменении одного пользователя можно не учитывать)
    4: Реализовать слои приложения для функциональности "еда". API контроллера должна удовлетворять все потребности демо приложения и ничего лишнего (см. демо).
        Смотрите на реализацию слоя для user и делаете по аналогии! Если там что-то непонятно, не надо исправлять или делать по своему. Задавайте вопросы. Если действительно нужна правка- я сделаю и напишу всем.
        4.1: после авторизации (сделаем позднее), id авторизованного юзера можно получить из SecurityUtil.authUserId(). Запрос попадает в контроллер, методы которого будут доступны снаружи по http, т.е. запрос можно будет сделать с ЛЮБЫМ id для еды (не принадлежащем авторизированному пользователю). Нельзя позволять модифицировать/смотреть чужую еду.
        4.2: SecurityUtil может использоваться только на слое web (см. реализацию ProfileRestController). MealService можно тестировать без подмены логики авторизации, принимаем в методах сервиса и репозитория параметр userId: id владельца еды.
        4.3: если еда не принадлежит авторизированному пользователю или отсутствует, в MealServiceImpl бросать NotFoundException.
        4.4: конвертацию в MealWithExceeded можно делать как в слое web, так и в service (Mapping Entity->DTO: Controller or Service?)
        4.5: в MealServiceImpl постараться сделать в каждом методе только одни запрос к MealRepository
        4.6 еще раз: не надо в названиях методов повторять названия класса (Meal).
    5: включить классы еды в контекст Spring (добавить аннотации) и вызвать из SpringMain любой метод MealRestController (проверить что Spring все корректно заинжектил)
